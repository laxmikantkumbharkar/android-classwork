package com.sunbeaminfo.myimdb.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.sunbeaminfo.myimdb.R;
import com.sunbeaminfo.myimdb.utils.Constants;
import com.sunbeaminfo.myimdb.utils.Utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProfileActivity extends BaseActivity {

    @BindView(R.id.imageView) ImageView imageView;
    @BindView(R.id.textUserName) TextView textUserName;
    @BindView(R.id.textEmail) TextView textEmail;
    @BindView(R.id.textPhone) TextView textPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getProfile();
    }

    private void getProfileImage(String fileName) {
        final String imageUrl = Utils.getImageUrl(fileName);
        Ion.with(this)
                .load(imageUrl)
                .withBitmap()
                .intoImageView(imageView);
    }

    private void getProfile() {
        final int userId = PreferenceManager.getDefaultSharedPreferences(this).getInt("user_id", 0);
        final String url = Utils.getUrl(Constants.ROUTE_USER + "/profile/" + userId);
        Ion.with(this)
                .load(url)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (result.get("status").getAsString().equals("success")) {
                            JsonObject user = result.get("data").getAsJsonObject();

                            textUserName.setText(user.get("name").getAsString());
                            textEmail.setText(user.get("email").getAsString());
                            textPhone.setText(user.get("phone").getAsString());

                            getProfileImage(user.get("profileImage").getAsString());
                        }
                    }
                });
    }

    @OnClick(R.id.imageView)
    public void onViewClicked() {
        Intent intent = new Intent();
        intent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {

            final int userId = PreferenceManager.getDefaultSharedPreferences(this).getInt("user_id", 0);

            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            try {
                FileOutputStream fileOutputStream = new FileOutputStream("/sdcard/tmpfile");
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
                fileOutputStream.close();

                final String url = Utils.getUrl(Constants.ROUTE_USER + "/profileImage/" + userId);
                Ion.with(this)
                        .load(url)
                        .setMultipartFile("photo", new File("/sdcard/tmpfile"))
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>() {
                            @Override
                            public void onCompleted(Exception e, JsonObject result) {
                                Log.d("result", result.toString());

                            }
                        });

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}
