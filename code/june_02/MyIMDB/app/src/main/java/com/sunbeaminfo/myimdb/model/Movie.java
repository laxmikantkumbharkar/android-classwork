package com.sunbeaminfo.myimdb.model;

public class Movie {
    private int id;
    private String title;
    private String shortDescription;
    private String directors;
    private String writers;
    private String stars;
    private float rating;
    private String storyline;
    private String thumbnail;
    private String genre;
    private int year;
    private String length;

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", shortDescription='" + shortDescription + '\'' +
                ", directors='" + directors + '\'' +
                ", writers='" + writers + '\'' +
                ", stars='" + stars + '\'' +
                ", rating=" + rating +
                ", storyline='" + storyline + '\'' +
                ", thumbnail='" + thumbnail + '\'' +
                ", genre='" + genre + '\'' +
                '}';
    }

    public Movie() {
    }

    public Movie(int id, String title, String shortDescription, String directors, String writers, String stars, float rating, String storyline, String thumbnail, String genre) {
        this.id = id;
        this.title = title;
        this.shortDescription = shortDescription;
        this.directors = directors;
        this.writers = writers;
        this.stars = stars;
        this.rating = rating;
        this.storyline = storyline;
        this.thumbnail = thumbnail;
        this.genre = genre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getDirectors() {
        return directors;
    }

    public void setDirectors(String directors) {
        this.directors = directors;
    }

    public String getWriters() {
        return writers;
    }

    public void setWriters(String writers) {
        this.writers = writers;
    }

    public String getStars() {
        return stars;
    }

    public void setStars(String stars) {
        this.stars = stars;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getStoryline() {
        return storyline;
    }

    public void setStoryline(String storyline) {
        this.storyline = storyline;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
}
