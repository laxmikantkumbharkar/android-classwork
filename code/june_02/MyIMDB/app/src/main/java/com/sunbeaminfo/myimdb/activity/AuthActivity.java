package com.sunbeaminfo.myimdb.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.sunbeaminfo.myimdb.R;
import com.sunbeaminfo.myimdb.fragment.LoginFragment;

public class AuthActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        getSupportActionBar().hide();

        addLoginScreen();
    }

    private void addLoginScreen() {
        LoginFragment loginFragment = new LoginFragment();
        getSupportFragmentManager()
            .beginTransaction()
            .replace(R.id.frameLayout, loginFragment)
            .commit();
    }
}
