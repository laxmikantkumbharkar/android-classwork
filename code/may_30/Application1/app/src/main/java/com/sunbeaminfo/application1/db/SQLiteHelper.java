package com.sunbeaminfo.application1.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static com.sunbeaminfo.application1.utils.Constants.*;

public class SQLiteHelper extends SQLiteOpenHelper {


    private static final String DB_NAME = "app_db.sqlite";
    private static final int DB_VERSION = 4;


    public SQLiteHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    // will be called only when, the db is not present
    @Override
    public void onCreate(SQLiteDatabase db) {
        // new users
        db.execSQL("create table " + TABLE_CONTACT + " ( "
                    + COL_ID + " integer primary key autoincrement, "
                    + COL_NAME + " TEXT, "
                    + COL_ADDRESS + " TEXT)");

        db.execSQL("create table " + TABLE_MOBILE + " ( "
                + COL_ID + " integer primary key autoincrement, "
                + COL_MODEL + " TEXT, "
                + COL_COMPANY + " TEXT)");

        db.execSQL("create table " + TABLE_COMPUTER + " ( "
                + COL_ID + " integer primary key autoincrement, "
                + COL_CPU + " TEXT, "
                + COL_RAM + " TEXT)");

        db.execSQL("create table " + TABLE_CAR + " ( "
                + COL_ID + " integer primary key autoincrement, "
                + COL_MODEL + " TEXT, "
                + COL_COMPANY + " TEXT)");
    }


    // called when
    // - the db exists
    // - the db version does NOT match
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // existing users


        if (oldVersion == 1 && newVersion == 4) {
            // from 1 to 4

            db.execSQL("create table " + TABLE_MOBILE + " ( "
                    + COL_ID + " integer primary key autoincrement, "
                    + COL_MODEL + " TEXT, "
                    + COL_COMPANY + " TEXT)");

            db.execSQL("create table " + TABLE_COMPUTER + " ( "
                    + COL_ID + " integer primary key autoincrement, "
                    + COL_CPU + " TEXT, "
                    + COL_RAM + " TEXT)");

            db.execSQL("create table " + TABLE_CAR + " ( "
                    + COL_ID + " integer primary key autoincrement, "
                    + COL_MODEL + " TEXT, "
                    + COL_COMPANY + " TEXT)");

        } else if (oldVersion == 2 && newVersion == 4) {
            // from 2 to 4

            db.execSQL("create table " + TABLE_COMPUTER + " ( "
                    + COL_ID + " integer primary key autoincrement, "
                    + COL_CPU + " TEXT, "
                    + COL_RAM + " TEXT)");

            db.execSQL("create table " + TABLE_CAR + " ( "
                    + COL_ID + " integer primary key autoincrement, "
                    + COL_MODEL + " TEXT, "
                    + COL_COMPANY + " TEXT)");

        } else if (oldVersion == 3 && newVersion == 4) {
            // from 3 to 4

            db.execSQL("create table " + TABLE_CAR + " ( "
                    + COL_ID + " integer primary key autoincrement, "
                    + COL_MODEL + " TEXT, "
                    + COL_COMPANY + " TEXT)");
        }
    }
}
