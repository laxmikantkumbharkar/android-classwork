package com.sunbeaminfo.application1.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.sunbeaminfo.application1.db.SQLiteHelper;
import static com.sunbeaminfo.application1.utils.Constants.*;

public class MyContentProvider extends ContentProvider {

    private static final int CODE_CONTACT = 1;
    private static final int CODE_MOBILE = 2;
    private static final int CODE_COMPUTER = 3;

    static UriMatcher uriMatcher;

    // static block
    // used to initialize static members

    // pattern matching technique
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

        // content://com.sunbeaminfo.application1/Contact  ->  1
        uriMatcher.addURI(AUTHORITY, PATH_CONTACT, CODE_CONTACT);

        // content://com.sunbeaminfo.application1/Mobile   ->  2
        uriMatcher.addURI(AUTHORITY, PATH_MOBILE, CODE_MOBILE);

        // content://com.sunbeaminfo.application1/Computer ->  2
        uriMatcher.addURI(AUTHORITY, PATH_COMPUTER, CODE_COMPUTER);
    }




    SQLiteDatabase db;

    @Override
    public boolean onCreate() {
        SQLiteHelper helper = new SQLiteHelper(getContext());
        db = helper.getWritableDatabase();
        return false;
    }

    
    @Override
    public Cursor query(Uri uri,  String[] projection,  String selection,  String[] selectionArgs,  String sortOrder) {
        int code = uriMatcher.match(uri);
        Cursor cursor = null;
        if (code == CODE_CONTACT) {
            cursor = db.query(TABLE_CONTACT, projection, selection, selectionArgs, null, null, sortOrder);
        } else if (code == CODE_MOBILE) {
            cursor = db.query(TABLE_MOBILE, projection, selection, selectionArgs, null, null, sortOrder);
        } else if (code == CODE_COMPUTER) {
            cursor = db.query(TABLE_COMPUTER, projection, selection, selectionArgs, null, null, sortOrder);
        }

        return cursor;
    }

    
    @Override
    public String getType(Uri uri) {
        return null;
    }

    
    @Override
    public Uri insert(Uri uri,  ContentValues values) {

        int code = uriMatcher.match(uri);
        if (code == CODE_CONTACT) {
            // content://com.sunbeaminfo.application1/Contact
            db.insert(TABLE_CONTACT, null, values);

        } else if (code == CODE_MOBILE) {
            // content://com.sunbeaminfo.application1/Mobile
            db.insert(TABLE_MOBILE, null, values);

        } else if (code == CODE_COMPUTER) {
            // content://com.sunbeaminfo.application1/Computer
            db.insert(TABLE_COMPUTER, null, values);

        }

        return null;
    }

    @Override
    public int delete(Uri uri,  String selection,  String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri,  ContentValues values,  String selection,  String[] selectionArgs) {
        return 0;
    }
}
