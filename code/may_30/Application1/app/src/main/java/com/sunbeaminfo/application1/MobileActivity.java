package com.sunbeaminfo.application1;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.sunbeaminfo.application1.model.Contact;
import com.sunbeaminfo.application1.model.Mobile;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.sunbeaminfo.application1.utils.Constants.*;

public class MobileActivity extends AppCompatActivity {

    @BindView(R.id.editModel) EditText editModel;
    @BindView(R.id.editCompany) EditText editCompany;
    @BindView(R.id.listView) ListView listView;

    ArrayList<Mobile> mobiles = new ArrayList<>();
    ArrayAdapter<Mobile> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile);
        ButterKnife.bind(this);

        adapter = new ArrayAdapter<Mobile>(this, android.R.layout.simple_list_item_1, mobiles);
        listView.setAdapter(adapter);
    }

    @OnClick(R.id.buttonInsert)
    public void onButtonInsertClicked() {

        ContentValues values = new ContentValues();
        values.put(COL_MODEL, editModel.getText().toString());
        values.put(COL_COMPANY, editCompany.getText().toString());

        getContentResolver().insert(CONTENT_URI_MOBILE, values);
    }

    @OnClick(R.id.buttonQuery)
    public void onButtonQueryClicked() {
        mobiles.clear();

        String columns[] = {COL_ID, COL_MODEL, COL_COMPANY};
        Cursor cursor = getContentResolver().query(CONTENT_URI_MOBILE, columns, null, null, null);
        if (!cursor.isAfterLast()) {
            cursor.moveToFirst();

            while (!cursor.isAfterLast()) {

                int id = cursor.getInt(cursor.getColumnIndex(COL_ID));
                String model = cursor.getString(cursor.getColumnIndex(COL_MODEL));
                String company = cursor.getString(cursor.getColumnIndex(COL_COMPANY));

                mobiles.add(new Mobile(id, model, company));

                cursor.moveToNext();
            }
        }
        cursor.close();

        adapter.notifyDataSetChanged();
    }
}
