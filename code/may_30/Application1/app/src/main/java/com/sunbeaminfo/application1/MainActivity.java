package com.sunbeaminfo.application1;

import android.app.Person;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.buttonPersons)
    public void onButtonPersonsClicked() {
        startActivity(new Intent(this, PersonActivity.class));
    }

    @OnClick(R.id.buttonMobiles)
    public void onButtonMobilesClicked() {
        startActivity(new Intent(this, MobileActivity.class));
    }
}
