package com.sunbeaminfo.application1.utils;

import android.net.Uri;

public class Constants {

    // related with contact table contact
    public static final String TABLE_CONTACT = "Contact";
    public static final String COL_ID = "Id";
    public static final String COL_NAME = "Name";
    public static final String COL_ADDRESS = "Address";


    // related with contact table mobile
    public static final String TABLE_MOBILE = "Mobile";
    public static final String COL_MODEL = "Model";
    public static final String COL_COMPANY = "Company";

    // related with contact table Car
    public static final String TABLE_CAR = "Car";


    // related with contact table Coputer
    public static final String TABLE_COMPUTER = "Computer";
    public static final String COL_CPU = "Cpu";
    public static final String COL_RAM = "RAM";

    // related to content provider
    public static final String SCHEME = "content://";
    public static final String PATH_CONTACT = "Contact";
    public static final String PATH_MOBILE = "Mobile";
    public static final String PATH_COMPUTER = "Computer";

    public static final String AUTHORITY = "com.sunbeaminfo.application1";

    // content://com.sunbeaminfo.application1
    public static final Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY);

    // content://com.sunbeaminfo.application1/Contact
    public static final Uri CONTENT_URI_CONTACT = Uri.parse(SCHEME + AUTHORITY + "/" + PATH_CONTACT);

    // content://com.sunbeaminfo.application1/Mobile
    public static final Uri CONTENT_URI_MOBILE = Uri.parse(SCHEME + AUTHORITY + "/" + PATH_MOBILE);

    // content://com.sunbeaminfo.application1/Computer
    public static final Uri CONTENT_URI_COMPUTER = Uri.parse(SCHEME + AUTHORITY + "/" + PATH_COMPUTER);

}
