package com.sunbeaminfo.application1;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.sunbeaminfo.application1.model.Contact;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.sunbeaminfo.application1.utils.Constants.COL_ADDRESS;
import static com.sunbeaminfo.application1.utils.Constants.COL_ID;
import static com.sunbeaminfo.application1.utils.Constants.COL_NAME;
import static com.sunbeaminfo.application1.utils.Constants.CONTENT_URI;
import static com.sunbeaminfo.application1.utils.Constants.CONTENT_URI_CONTACT;

public class PersonActivity extends AppCompatActivity {

    ArrayList<Contact> contacts = new ArrayList<>();

    @BindView(R.id.editName) EditText editName;
    @BindView(R.id.editAddress) EditText editAddress;
    @BindView(R.id.listView) ListView listView;

    ArrayAdapter<Contact> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person);
        ButterKnife.bind(this);

        adapter = new ArrayAdapter<Contact>(this, android.R.layout.simple_list_item_1, contacts);
        listView.setAdapter(adapter);
    }

    @OnClick(R.id.buttonInsert)
    public void onButtonInsertClicked() {
        ContentResolver resolver = getContentResolver();

        ContentValues values = new ContentValues();
        values.put(COL_NAME, editName.getText().toString());
        values.put(COL_ADDRESS, editAddress.getText().toString());

        resolver.insert(CONTENT_URI_CONTACT, values);
    }

    @OnClick(R.id.buttonQuery)
    public void onButtonQueryClicked() {
        contacts.clear();

        String columns[] = {COL_ID, COL_NAME, COL_ADDRESS};
        Cursor cursor = getContentResolver().query(CONTENT_URI_CONTACT, columns, null, null, null);
        if (!cursor.isAfterLast()) {
            cursor.moveToFirst();

            while (!cursor.isAfterLast()) {

                int id = cursor.getInt(cursor.getColumnIndex(COL_ID));
                String name = cursor.getString(cursor.getColumnIndex(COL_NAME));
                String address = cursor.getString(cursor.getColumnIndex(COL_ADDRESS));

                contacts.add(new Contact(id, name, address));

                cursor.moveToNext();
            }
        }
        cursor.close();

        adapter.notifyDataSetChanged();
    }
}
