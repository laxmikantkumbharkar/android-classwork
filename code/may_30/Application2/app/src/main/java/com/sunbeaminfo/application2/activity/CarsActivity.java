package com.sunbeaminfo.application2.activity;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.sunbeaminfo.application2.R;
import com.sunbeaminfo.application2.model.Car;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.sunbeaminfo.application2.utils.Constants.COL_COMPANY;
import static com.sunbeaminfo.application2.utils.Constants.COL_ID;
import static com.sunbeaminfo.application2.utils.Constants.COL_MODEL;
import static com.sunbeaminfo.application2.utils.Constants.CONTENT_URI_CAR;
import static com.sunbeaminfo.application2.utils.Constants.CONTENT_URI_CONTACT;

public class CarsActivity extends AppCompatActivity {

    @BindView(R.id.listView) ListView listView;

    ArrayList<Car> cars = new ArrayList<>();
    ArrayAdapter<Car> adapter;
    @BindView(R.id.editModel) EditText editModel;
    @BindView(R.id.editCompany) EditText editCompany;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cars);
        ButterKnife.bind(this);

        adapter = new ArrayAdapter<Car>(this, android.R.layout.simple_list_item_1, cars);
        listView.setAdapter(adapter);

        registerForContextMenu(listView);
    }

    @OnClick(R.id.buttonInsert)
    public void onButtonInsertClicked() {
        ContentValues values = new ContentValues();
        values.put(COL_MODEL, editModel.getText().toString());
        values.put(COL_COMPANY, editCompany.getText().toString());

        getContentResolver().insert(CONTENT_URI_CAR, values);
    }

    @OnClick(R.id.buttonQuery)
    public void onButtonQueryClicked() {
        cars.clear();

        String columns[] = {COL_ID, COL_MODEL, COL_COMPANY};
        Cursor cursor = getContentResolver().query(CONTENT_URI_CAR, columns, null, null, null);
        if (!cursor.isAfterLast()) {
            cursor.moveToFirst();

            while (!cursor.isAfterLast()) {

                Car car = new Car();
                car.setId(cursor.getInt(cursor.getColumnIndex(COL_ID)));
                car.setModel(cursor.getString(cursor.getColumnIndex(COL_MODEL)));
                car.setCompany(cursor.getString(cursor.getColumnIndex(COL_COMPANY)));

                cars.add(car);

                cursor.moveToNext();
            }
        }
        cursor.close();

        adapter.notifyDataSetChanged();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add("Delete");
        menu.add("Details");
        menu.add("Update");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo menuInfo =
                (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        Car car = cars.get(menuInfo.position);

        if (item.getTitle().equals("Delete")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("confirmation..");
            builder.setMessage("Are you sure you want to delete ?");
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    String where = COL_ID + " = ?";
                    String whereArgs[] = new String[] { "" + car.getId() };

                    getContentResolver().delete(CONTENT_URI_CAR, where, whereArgs);

                    // for refreshing the listView
                    onButtonQueryClicked();
                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.show();
        } else if (item.getTitle().equals("Details")) {

        } else if (item.getTitle().equals("Update")) {

        }
        return super.onContextItemSelected(item);
    }
}
