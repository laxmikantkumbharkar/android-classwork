package com.sunbeaminfo.application2.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.sunbeaminfo.application2.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.buttonContacts)
    public void onButtonContactsClicked() {
         startActivity(new Intent(this, ContactsActivity.class));
    }

    @OnClick(R.id.buttonCars)
    public void onButtonCarsClicked() {
        startActivity(new Intent(this, CarsActivity.class));
    }
}
