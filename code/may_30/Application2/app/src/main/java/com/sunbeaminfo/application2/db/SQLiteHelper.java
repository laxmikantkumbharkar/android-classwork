package com.sunbeaminfo.application2.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static  com.sunbeaminfo.application2.utils.Constants.*;

public class SQLiteHelper extends SQLiteOpenHelper {


    private static final int DB_VERSION = 2;
    private static final String DB_NAME = "mydb.sqlite";

    public SQLiteHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
            "create table " + TABLE_CONTACT + " ( "
                + COL_ID + " integer primary key autoincrement, "
                + COL_NAME + " TEXT, "
                + COL_ADDRESS + " TEXT) "
        );

        db.execSQL(
                "create table " + TABLE_CAR + " ( "
                        + COL_ID + " integer primary key autoincrement, "
                        + COL_MODEL + " TEXT, "
                        + COL_COMPANY + " TEXT) "
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        if (oldVersion == 1 && newVersion == 2) {
            db.execSQL(
                    "create table " + TABLE_CAR + " ( "
                            + COL_ID + " integer primary key autoincrement, "
                            + COL_MODEL + " TEXT, "
                            + COL_COMPANY + " TEXT) "
            );
        }
    }
}
