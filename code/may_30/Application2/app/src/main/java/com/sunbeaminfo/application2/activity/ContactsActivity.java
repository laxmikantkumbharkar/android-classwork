package com.sunbeaminfo.application2.activity;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.sunbeaminfo.application2.R;
import com.sunbeaminfo.application2.model.Contact;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.sunbeaminfo.application2.utils.Constants.*;

public class ContactsActivity extends AppCompatActivity {

    @BindView(R.id.editName) EditText editName;
    @BindView(R.id.editAddress) EditText editAddress;
    @BindView(R.id.listView) ListView listView;

    ArrayList<Contact> contacts = new ArrayList<>();
    ArrayAdapter<Contact> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);
        ButterKnife.bind(this);

        adapter = new ArrayAdapter<Contact>(this, android.R.layout.simple_list_item_1, contacts);
        listView.setAdapter(adapter);

        // when user clicks and holds for a few seconds the context menu will appear
        registerForContextMenu(listView);
    }

    @OnClick(R.id.buttonInsert)
    public void onButtonInsertClicked() {
        ContentValues values = new ContentValues();
        values.put(COL_NAME, editName.getText().toString());
        values.put(COL_ADDRESS, editAddress.getText().toString());

        getContentResolver().insert(CONTENT_URI_CONTACT, values);
    }

    @OnClick(R.id.buttonQuery)
    public void onButtonQueryClicked() {
        contacts.clear();

        String columns[] = { COL_ID, COL_NAME, COL_ADDRESS };
        Cursor cursor = getContentResolver().query(CONTENT_URI_CONTACT, columns, null, null, null);
        if (!cursor.isAfterLast()) {
            cursor.moveToFirst();

            while(!cursor.isAfterLast()) {

                Contact contact = new Contact();
                contact.setId(cursor.getInt(cursor.getColumnIndex(COL_ID)));
                contact.setName(cursor.getString(cursor.getColumnIndex(COL_NAME)));
                contact.setAddress(cursor.getString(cursor.getColumnIndex(COL_ADDRESS)));

                contacts.add(contact);

                cursor.moveToNext();
            }
        }
        cursor.close();

        adapter.notifyDataSetChanged();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        menu.add("Delete");
        menu.add("Details");
        menu.add("Update");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo menuInfo =
                (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        Contact contact = contacts.get(menuInfo.position);

        if (item.getTitle().equals("Delete")) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("confirmation..");
            builder.setMessage("Are you sure you want to delete ?");
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    String where = COL_ID + " = ?";
                    String whereArgs[] = new String[] { "" + contact.getId() };

                    getContentResolver().delete(CONTENT_URI_CONTACT, where, whereArgs);

                    // for refreshing the listView
                    onButtonQueryClicked();
                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.show();

        } else if (item.getTitle().equals("Details")) {

        } else if (item.getTitle().equals("Update")) {
            Intent intent = new Intent(this, UpdateContactActivity.class);
            intent.putExtra("id", contact.getId());
            startActivity(intent);
        }

        return super.onContextItemSelected(item);
    }
}
