package com.sunbeaminfo.application2.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.sunbeaminfo.application2.db.SQLiteHelper;
import static com.sunbeaminfo.application2.utils.Constants.*;

public class MyContentProvider extends ContentProvider {

    SQLiteDatabase db;

    static UriMatcher uriMatcher;

    private static final int CODE_CONTACT = 1;

    private static final int CODE_CAR = 2;

    private static final int CODE_CONTACT_ID = 3;

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

        // content://com.sunbeaminfo.application2/Contact
        uriMatcher.addURI(AUTHORITY, PATH_CONTACT, CODE_CONTACT);

        // content://com.sunbeaminfo.application2/Car
        uriMatcher.addURI(AUTHORITY, PATH_CAR, CODE_CAR);

        // content://com.sunbeaminfo.application2/Contact/<id>
        // # will match with any contact id
        uriMatcher.addURI(AUTHORITY, PATH_CONTACT + "/#", CODE_CONTACT_ID);
    }

    public MyContentProvider() {
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int code = uriMatcher.match(uri);
        if (code == CODE_CONTACT) {
            db.delete(TABLE_CONTACT, selection, selectionArgs);
        } else if (code == CODE_CAR) {
            db.delete(TABLE_CAR, selection, selectionArgs);
        }

        return 0;
    }

    @Override
    public String getType(Uri uri) {
        // TODO: Implement this to handle requests for the MIME type of the data
        // at the given URI.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        int code = uriMatcher.match(uri);
        if (code == CODE_CONTACT) {
            db.insert(TABLE_CONTACT, null, values);
        } else if (code == CODE_CAR) {
            db.insert(TABLE_CAR, null, values);
        }

        return null;
    }

    @Override
    public boolean onCreate() {
        SQLiteHelper helper = new SQLiteHelper(getContext());
        db = helper.getWritableDatabase();
        return false;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor cursor = null;
        int code = uriMatcher.match(uri);

        if (code == CODE_CONTACT) {
            cursor = db.query(TABLE_CONTACT, projection, selection, selectionArgs, null, null, sortOrder);

        } else if (code == CODE_CAR) {
            cursor = db.query(TABLE_CAR, projection, selection, selectionArgs, null, null, sortOrder);

        } else if (code == CODE_CONTACT_ID) {

            // content://com.sunbeaminfo.application2/Contact/<id>
            // ["Contact", "<id>"]
            String id = uri.getPathSegments().get(1);


            cursor = db.query(TABLE_CONTACT, projection, COL_ID + " = ?", new String[] { id }, null, null, sortOrder);
        }

        return cursor;
    }


    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int code = uriMatcher.match(uri);
        if (code == CODE_CONTACT) {
            db.update(TABLE_CONTACT, values, selection, selectionArgs);
        } else if (code == CODE_CAR) {
            db.update(TABLE_CAR, values, selection, selectionArgs);
        }

        return 0;
    }
}
