package com.sunbeaminfo.application2.utils;

import android.net.Uri;

public class Constants {

    // related to Contacts
    public static final String TABLE_CONTACT = "Contact";
    public static final String COL_ID = "Id";
    public static final String COL_NAME = "Name";
    public static final String COL_ADDRESS = "Address";

    // related to Cars
    public static final String TABLE_CAR = "Car";
    public static final String COL_MODEL = "Model";
    public static final String COL_COMPANY = "Company";


    // related to ContentProvider
    public static final String SCHEME = "content://";
    public static final String AUTHORITY = "com.sunbeaminfo.application2";
    public static final String PATH_CAR = "Car";
    public static final String PATH_CONTACT = "Contact";

    public static final Uri CONTENT_URI_CONTACT = Uri.parse(SCHEME + AUTHORITY + "/" + PATH_CONTACT);
    public static final Uri CONTENT_URI_CAR = Uri.parse(SCHEME + AUTHORITY + "/" + PATH_CAR);
}
