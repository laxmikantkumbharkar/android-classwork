package com.sunbeaminfo.application2.activity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import com.sunbeaminfo.application2.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.sunbeaminfo.application2.utils.Constants.*;

public class UpdateContactActivity extends AppCompatActivity {

    @BindView(R.id.editName) EditText editName;
    @BindView(R.id.editAddress) EditText editAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_contact);
        ButterKnife.bind(this);


        Intent intent = getIntent();
        int id = intent.getIntExtra("id", 0);

        // content://com.sunbeaminfo.application2/Contact/<id>
        Uri uri = Uri.parse(SCHEME + AUTHORITY + "/" + PATH_CONTACT + "/" + id);

        String columns[] = { COL_ID, COL_NAME, COL_ADDRESS };
        Cursor cursor = getContentResolver().query(uri, columns, null, null, null);

        if (!cursor.isAfterLast()) {
            cursor.moveToFirst();

            editName.setText(cursor.getString(cursor.getColumnIndex(COL_NAME)));
            editAddress.setText(cursor.getString(cursor.getColumnIndex(COL_ADDRESS)));
        }

        cursor.close();
    }

    @OnClick(R.id.buttonUpdate)
    public void onButtonUpdateClicked() {
        ContentValues values = new ContentValues();
        values.put(COL_NAME, editName.getText().toString());
        values.put(COL_ADDRESS, editAddress.getText().toString());


        Intent intent = getIntent();
        int id = intent.getIntExtra("id", 0);

        getContentResolver().update(CONTENT_URI_CONTACT, values, COL_ID + " = ?", new String[] { "" + id });

        finish();
    }

    @OnClick(R.id.buttonCancel)
    public void onButtonCancelClicked() {
        finish();
    }
}
