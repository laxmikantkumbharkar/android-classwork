package com.sunbeaminfo.application2;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    ImageView imageView;
    EditText editData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = findViewById(R.id.imageView);
        editData = findViewById(R.id.editData);
    }

    public void dialNumber(View v) {
        // scheme: tel://
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel://" + editData.getText().toString()));
        startActivity(intent);
    }

    public void browseWeb(View v) {
        // scheme: http or https
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(editData.getText().toString()));
        startActivity(intent);
    }

    public void makeCall(View v) {
        // scheme: tel://
//        if (ContextCompat.checkSelfPermission(this, "android.intent.action.CALL") ==
//                PackageManager.PERMISSION_GRANTED) {
//            call();
//        } else {
//            ActivityCompat.requestPermissions(this, new String[] {
//                    "android.intent.action.CALL"
//            }, 0);
//        }
        call();
    }

    private void call() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel://" +  editData.getText().toString()));
        startActivity(intent);
    }

    public void openTestActivity(View v) {
        // Explicit intent
//        Intent intent = new Intent(this, TestActivity.class);
//        startActivity(intent);

        // Explicit intent
        Intent intent = new Intent();
        intent.setClassName("com.sunbeaminfo.application2", "com.sunbeaminfo.application2.TestActivity");
        startActivity(intent);
    }

    public void openApp1MainActivity(View v) {
        // Explicit intent
        Intent intent = new Intent();
        intent.setClassName("com.sunbeaminfo.application1", "com.sunbeaminfo.application1.MainActivity");
        startActivity(intent);
    }

    public void takePhoto(View v) {
        // implicit intent
        Intent intent = new Intent();
        intent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 0);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        Log.d("permissions", "" + permissions[0]);
        Log.d("permissions", "" + grantResults[0]);

        if (grantResults.length > 0 &&
            grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            call();
        } else {
            Toast.makeText(this, "I need the permission", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            imageView.setImageBitmap(bitmap);
        }
    }
}


















