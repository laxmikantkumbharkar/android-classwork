package com.sunbeaminfo.application4.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.sunbeaminfo.application4.R;

public class PersonsListFragment extends Fragment implements AdapterView.OnItemClickListener {

    String persons[] = {"Steve Jobs", "Bill Gates", "Bill Joy", "Dennis", "BS", "James", "Chris", "Tom and Brad", "Brandan", "Sir Tim"};
    ArrayAdapter<String> adapter;
    ListView listView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.fragment, null);

        listView = layout.findViewById(R.id.listView);
        adapter  = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, persons);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);

        return layout;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Toast.makeText(getActivity(), "selected person: " + persons[position], Toast.LENGTH_SHORT).show();
    }
}