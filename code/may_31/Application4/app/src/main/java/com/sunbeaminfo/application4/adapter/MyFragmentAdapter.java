package com.sunbeaminfo.application4.adapter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.sunbeaminfo.application4.fragment.CarsListFragment;
import com.sunbeaminfo.application4.fragment.MobilesListFragment;
import com.sunbeaminfo.application4.fragment.PersonsListFragment;

public class MyFragmentAdapter extends FragmentPagerAdapter {

    CarsListFragment carsListFragment = new CarsListFragment();
    PersonsListFragment personsListFragment = new PersonsListFragment();
    MobilesListFragment mobilesListFragment = new MobilesListFragment();
    CarsListFragment carsListFragment2 = new CarsListFragment();
    PersonsListFragment personsListFragment2 = new PersonsListFragment();
    MobilesListFragment mobilesListFragment2 = new MobilesListFragment();

    public MyFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        Fragment fragment = null;
        if (i == 0) {
            fragment = carsListFragment;
        } else if (i == 1) {
            fragment = personsListFragment;
        } else if (i == 2) {
            fragment = mobilesListFragment;
        } else if (i == 3) {
            fragment = carsListFragment2;
        } else if (i == 4) {
            fragment = personsListFragment2;
        } else if (i == 5) {
            fragment = mobilesListFragment2;
        }
        return fragment;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        String title = "";
        if (position == 0) {
            title = "Cars";
        } else if (position == 1) {
            title = "Persons";
        } else if (position == 2) {
            title = "Mobiles";
        } else if (position == 3) {
            title = "Cars";
        } else if (position == 4) {
            title = "Persons";
        } else if (position == 5) {
            title = "Mobiles";
        }
        return title;
    }



    @Override
    public int getCount() {
        return 6;
    }
}
