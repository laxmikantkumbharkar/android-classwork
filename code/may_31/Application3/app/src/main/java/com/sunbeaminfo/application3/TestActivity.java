package com.sunbeaminfo.application3;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.sunbeaminfo.application3.fragment.FirstFragment;
import com.sunbeaminfo.application3.fragment.SecondFragment;

public class TestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
    }

    public void showFirst(View v) {
        // step 1: instantiate the fragment
        FirstFragment firstFragment = new FirstFragment();

        // step 2: get the fragment manager
        FragmentManager fragmentManager = getSupportFragmentManager();

        // step 3: create a fragment  transaction
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        // step 4: perform the operation [add/remove/replace]
        transaction.replace(R.id.layoutFrame, firstFragment);

        // step 5: commit the transaction
        transaction.commit();
    }

    public void showSecond(View v) {
        SecondFragment secondFragment = new SecondFragment();

        getSupportFragmentManager()
            .beginTransaction()
            .replace(R.id.layoutFrame, secondFragment)
            .commit();
    }
}
