package com.sunbeaminfo.application3.activity;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.sunbeaminfo.application3.R;
import com.sunbeaminfo.application3.db.SQLiteHelper;


public class AddContactActivity extends AppCompatActivity {

    EditText editFirstName, editLastName, editEmail, editAddress, editPhone, editZipcode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);

        editFirstName = findViewById(R.id.editFirstName);
        editLastName = findViewById(R.id.editLastName);
        editEmail = findViewById(R.id.editEmail);
        editAddress = findViewById(R.id.editAddress);
        editPhone = findViewById(R.id.editPhone);
        editZipcode = findViewById(R.id.editZipcode);

        // for back arrow on the left top corner
        ActionBar actionBar = getSupportActionBar();
        // actionBar.setTitle("Changed");
        actionBar.setDisplayHomeAsUpEnabled(true);
    }


    private void insertContact() {
        SQLiteHelper helper = new SQLiteHelper(this);
        SQLiteDatabase db = helper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("firstName", editFirstName.getText().toString());
        values.put("lastName", editLastName.getText().toString());
        values.put("email", editEmail.getText().toString());
        values.put("phone", editPhone.getText().toString());
        values.put("address", editAddress.getText().toString());
        values.put("zipcode", editZipcode.getText().toString());

        db.insert("Contact", null, values);

        db.close();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_contact, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int id = item.getItemId();
        if (id == R.id.menuSave) {
            if (editFirstName.getText().toString().length() == 0) {
                Toast.makeText(this, "enter first name", Toast.LENGTH_SHORT).show();
            } else if (editLastName.getText().toString().length() == 0) {
                Toast.makeText(this, "enter last name", Toast.LENGTH_SHORT).show();
            } else {
                insertContact();
                finish();
            }
        } else if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
