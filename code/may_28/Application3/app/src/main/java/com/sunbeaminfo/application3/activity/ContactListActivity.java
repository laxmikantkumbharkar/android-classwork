package com.sunbeaminfo.application3.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.sunbeaminfo.application3.R;
import com.sunbeaminfo.application3.adapter.ContactAdapter;
import com.sunbeaminfo.application3.db.SQLiteHelper;
import com.sunbeaminfo.application3.model.Contact;

import java.util.ArrayList;


public class ContactListActivity extends AppCompatActivity implements ContactAdapter.Listener {

    RecyclerView recyclerView;
    ArrayList<Contact> contacts = new ArrayList<>();
    ContactAdapter contactAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 1));

        contactAdapter = new ContactAdapter(this, contacts);
        recyclerView.setAdapter(contactAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();

        getContacts();
    }

    private void getContacts() {
        contacts.clear();

        SQLiteHelper helper = new SQLiteHelper(this);
        SQLiteDatabase db = helper.getReadableDatabase();

        String columns[] = { "id", "firstName", "lastName", "email", "address", "zipcode", "phone" };
        Cursor cursor = db.query("Contact", columns, null, null, null, null, null);

        if (!cursor.isAfterLast()) {
            cursor.moveToFirst();

            while(!cursor.isAfterLast()) {
                Contact contact = new Contact();
                contact.setId(cursor.getInt(cursor.getColumnIndex("id")));
                contact.setFirstName(cursor.getString(cursor.getColumnIndex("firstName")));
                contact.setLastName(cursor.getString(cursor.getColumnIndex("lastName")));
                contact.setAddress(cursor.getString(cursor.getColumnIndex("address")));
                contact.setEmail(cursor.getString(cursor.getColumnIndex("email")));
                contact.setPhone(cursor.getString(cursor.getColumnIndex("phone")));
                contact.setZipcode(cursor.getInt(cursor.getColumnIndex("zipcode")));

                contacts.add(contact);
                cursor.moveToNext();
            }
        }

        db.close();

        // refresh recyclerview
        contactAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_contact_list, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menuAdd) {
            Intent intent = new Intent(this, AddContactActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onContactClick(int index) {
        Intent intent = new Intent(this, ContactDetailsActivity.class);
        intent.putExtra("contact", contacts.get(index));
        startActivity(intent);
    }

    @Override
    public void onEdit(int index) {
        Contact contact = contacts.get(index);
        Intent intent = new Intent(this, EditContactActivity.class);
        intent.putExtra("contact", contact);
        startActivity(intent);
    }

    @Override
    public void onDelete(final int index) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("confirmation...");
        builder.setMessage("Are you sure you want to remove this contact?");

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Contact contact = contacts.get(index);

                SQLiteHelper helper = new SQLiteHelper(ContactListActivity.this);
                SQLiteDatabase db = helper.getReadableDatabase();
                db.delete("Contact", "id = ?", new String[] { "" + contact.getId() });
                db.close();

                // refresh the recycler view
                getContacts();
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.show();
    }
}
