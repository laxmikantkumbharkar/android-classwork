package com.sunbeaminfo.application3.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sunbeaminfo.application3.R;
import com.sunbeaminfo.application3.model.Contact;

import java.util.ArrayList;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ViewHolder> {

    public interface Listener {
        void onContactClick(int index);
        void onEdit(int index);
        void onDelete(int index);
    }

    private final Context context;
    private final ArrayList<Contact> contacts;

    private final Listener listener;

    public ContactAdapter(Context context, ArrayList<Contact> contacts) {
        this.context = context;
        this.contacts = contacts;
        this.listener = (Listener) context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LinearLayout layout = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.recycler_item_contact, null);
        return new ViewHolder(layout);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        Contact contact = contacts.get(i);
        viewHolder.textName.setText(contact.getFirstName() + " " +  contact.getLastName());
        viewHolder.textAddress.setText(contact.getAddress() + ", " +  contact.getZipcode());
        viewHolder.textEmail.setText("Email: " + contact.getEmail());
        viewHolder.textPhone.setText("Phone: " + contact.getPhone());

        // if user clicks on the item
        // open the contact details
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onContactClick(i);
            }
        });

        viewHolder.imageViewEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onEdit(i);
            }
        });

        viewHolder.imageViewDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onDelete(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        View itemView;

        TextView textName, textAddress, textEmail, textPhone;
        ImageView imageViewEdit, imageViewDelete;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.itemView = itemView;
            this.textName = itemView.findViewById(R.id.textName);
            this.textAddress = itemView.findViewById(R.id.textAddress);
            this.textEmail = itemView.findViewById(R.id.textEmail);
            this.textPhone = itemView.findViewById(R.id.textPhone);
            this.imageViewEdit = itemView.findViewById(R.id.imageViewEdit);
            this.imageViewDelete = itemView.findViewById(R.id.imageViewDelete);
        }
    }
}
