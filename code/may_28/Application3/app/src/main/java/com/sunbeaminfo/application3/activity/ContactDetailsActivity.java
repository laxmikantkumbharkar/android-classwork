package com.sunbeaminfo.application3.activity;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.sunbeaminfo.application3.R;
import com.sunbeaminfo.application3.model.Contact;

public class ContactDetailsActivity extends AppCompatActivity {

    TextView textName, textAddress, textEmail, textPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_details);

        this.textName = findViewById(R.id.textName);
        this.textAddress = findViewById(R.id.textAddress);
        this.textEmail = findViewById(R.id.textEmail);
        this.textPhone = findViewById(R.id.textPhone);

        Intent intent = getIntent();
        Contact contact = (Contact) intent.getSerializableExtra("contact");
        textName.setText(contact.getFirstName() + " " +  contact.getLastName());
        textAddress.setText(contact.getAddress() + ", " +  contact.getZipcode());
        textEmail.setText("Email: " + contact.getEmail());
        textPhone.setText("Phone: " + contact.getPhone());

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
