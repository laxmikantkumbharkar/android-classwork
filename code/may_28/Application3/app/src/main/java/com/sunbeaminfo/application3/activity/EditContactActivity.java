package com.sunbeaminfo.application3.activity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.sunbeaminfo.application3.R;
import com.sunbeaminfo.application3.db.SQLiteHelper;
import com.sunbeaminfo.application3.model.Contact;

public class EditContactActivity extends AppCompatActivity {

    EditText editFirstName, editLastName, editEmail, editAddress, editPhone, editZipcode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_contact);

        editFirstName = findViewById(R.id.editFirstName);
        editLastName = findViewById(R.id.editLastName);
        editEmail = findViewById(R.id.editEmail);
        editAddress = findViewById(R.id.editAddress);
        editPhone = findViewById(R.id.editPhone);
        editZipcode = findViewById(R.id.editZipcode);

        // for back arrow on the left top corner
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        Contact contact = (Contact) intent.getSerializableExtra("contact");

        editFirstName.setText(contact.getFirstName());
        editLastName.setText(contact.getLastName());
        editEmail.setText(contact.getEmail());
        editAddress.setText(contact.getAddress());
        editPhone.setText(contact.getPhone());
        editZipcode.setText("" + contact.getZipcode());
    }

    private void updateContact() {
        Intent intent = getIntent();
        Contact contact = (Contact) intent.getSerializableExtra("contact");

        SQLiteHelper helper = new SQLiteHelper(this);
        SQLiteDatabase db = helper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("firstName", editFirstName.getText().toString());
        values.put("lastName", editLastName.getText().toString());
        values.put("email", editEmail.getText().toString());
        values.put("phone", editPhone.getText().toString());
        values.put("address", editAddress.getText().toString());
        values.put("zipcode", editZipcode.getText().toString());

        db.update("Contact", values, "id = ?", new String[] {"" + contact.getId()});

        db.close();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_contact, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == R.id.menuSave) {
            updateContact();
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
