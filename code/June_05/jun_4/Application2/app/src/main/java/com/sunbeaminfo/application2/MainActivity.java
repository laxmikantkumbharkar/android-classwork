package com.sunbeaminfo.application2;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.sunbeaminfo.application2.service.MyService;

public class MainActivity extends AppCompatActivity {

    MyService myService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d("MainActivity", "onServiceConnected()");
            MyService.MyBinder myBinder = (MyService.MyBinder) service;
            myService = myBinder.getServiceInstance();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d("MainActivity", "onServiceDisconnected()");
        }
    };

    public void startMyService(View v) {
        Intent intent = new Intent(this, MyService.class);
        bindService(intent, connection, BIND_AUTO_CREATE);
    }

    public void stopMyService(View v) {
        unbindService(connection);
    }

    public void downloadFile(View v) {
         myService.downloadFileUsingService();
    }
}
