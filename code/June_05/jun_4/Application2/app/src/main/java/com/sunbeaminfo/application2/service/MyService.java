package com.sunbeaminfo.application2.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

public class MyService extends Service {

    public class MyBinder extends Binder {

        public MyService getServiceInstance() {
            return MyService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("MyService", "onCreate()");
    }

    public void downloadFileUsingService() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Log.d("MyService", "downloading file..");
                    Thread.sleep(5000);
                    Log.d("MyService", "downloading finished..");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d("MyService", "onBind()");
        return new MyBinder();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d("MyService", "onUnbind()");
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("MyService", "onDestroy()");
    }
}
