package com.sunbeaminfo.application4;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void sendMyNotification(View v) {

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "channel_1");
        builder.setContentTitle("new movie added");
        builder.setContentText("Application has added new movie...........");
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setTicker("new movie available");
        builder.setPriority(NotificationCompat.PRIORITY_DEFAULT);


        Intent intent = new Intent(this, NotificationActivityActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        builder.setContentIntent(pendingIntent);

        NotificationChannel channel = new NotificationChannel("channel_1", "channel_1", NotificationManager.IMPORTANCE_DEFAULT);
        channel.setDescription("this is a default channel");

        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        manager.createNotificationChannel(channel);
        manager.notify(1, builder.build());
    }
}
