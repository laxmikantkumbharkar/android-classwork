package com.sunbeaminfo.application1.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class MyService extends Service {

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("MyService", "onCreate()");
    }

    private void downloadFile() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Log.d("MyService", "downloading file...");
                    Thread.sleep(5000);
                    Log.d("MyService", "downloading finished...");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d("MyService", "onBind()");

        // download the file
        // immediately when the service starts
        downloadFile();
        return null;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d("MyService", "onUnbind()");
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("MyService", "onDestroy()");
    }
}
