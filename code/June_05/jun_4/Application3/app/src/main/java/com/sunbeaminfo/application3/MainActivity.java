package com.sunbeaminfo.application3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.sunbeaminfo.application3.service.MyService;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void startMyService(View v) {
        startService(new Intent(this, MyService.class));
    }

    public void stopMyService(View v) {
        stopService(new Intent(this, MyService.class));
    }
}
