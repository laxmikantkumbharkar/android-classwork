package com.sunbeaminfo.application2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class SecondActivity extends AppCompatActivity {

    EditText editName, editAddress, editPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        editName = findViewById(R.id.editName);
        editAddress = findViewById(R.id.editAddress);
        editPhone = findViewById(R.id.editPhone);
    }

    public void sendData(View view) {

        // step 2: create intent, add the data and call setResult();
        // send the data to the previous activity
        Intent data = new Intent();
        data.putExtra("name", editName.getText().toString());
        data.putExtra("address", editAddress.getText().toString());
        data.putExtra("phone", editPhone.getText().toString());
        setResult(0, data);

        finish();
    }

    public void cancel(View view) {
        Intent data = new Intent();
        setResult(0, data);
        finish();
    }
}
