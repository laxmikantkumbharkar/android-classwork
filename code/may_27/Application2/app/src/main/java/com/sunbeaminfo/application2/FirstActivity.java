package com.sunbeaminfo.application2;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class FirstActivity extends AppCompatActivity {

    TextView textName, textAddress, textPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textName = findViewById(R.id.textName);
        textAddress = findViewById(R.id.textAddress);
        textPhone = findViewById(R.id.textPhone);
    }

    public void get(View v) {
        Intent intent = new Intent(this, SecondActivity.class);

        // step 1: use startActivityForResult instead of startActivity()
        startActivityForResult(intent, 0);
    }

    // step 3: override onActivityResult()
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            textName.setText("Name: " + data.getStringExtra("name"));
            textAddress.setText("Address: " + data.getStringExtra("address"));
            textPhone.setText("Phone: " + data.getStringExtra("phone"));
        }
    }
}
