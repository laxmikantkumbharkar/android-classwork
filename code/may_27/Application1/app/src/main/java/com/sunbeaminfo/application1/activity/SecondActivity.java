package com.sunbeaminfo.application1.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.sunbeaminfo.application1.R;
import com.sunbeaminfo.application1.model.Person;

public class SecondActivity extends AppCompatActivity {

    TextView textName, textAddress, textPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        textName = findViewById(R.id.textName);
        textAddress = findViewById(R.id.textAddress);
        textPhone = findViewById(R.id.textPhone);

        Intent intent = getIntent();
//        textName.setText("Name: " + intent.getStringExtra("name"));
//        textAddress.setText("Address: " + intent.getStringExtra("address"));

        Person person = (Person) intent.getSerializableExtra("person");
        textName.setText("Name: " + person.getName());
        textAddress.setText("Address: " + person.getAddress());
        textPhone.setText("Phone: " + person.getPhone());
    }

    public void back(View v) {
        finish();
    }

}
