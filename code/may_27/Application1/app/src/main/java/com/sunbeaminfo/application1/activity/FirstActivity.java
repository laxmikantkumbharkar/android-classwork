package com.sunbeaminfo.application1.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.sunbeaminfo.application1.R;
import com.sunbeaminfo.application1.model.Person;

public class FirstActivity extends AppCompatActivity {

    EditText editName, editAddress, editPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);

        editName = findViewById(R.id.editName);
        editAddress = findViewById(R.id.editAddress);
        editPhone = findViewById(R.id.editPhone);
    }

    public void sendData(View view) {

        Person person = new Person();
        person.setName(editName.getText().toString());
        person.setAddress(editAddress.getText().toString());
        person.setPhone(editPhone.getText().toString());

        // explicit intent
        Intent intent = new Intent(this, SecondActivity.class);
//        intent.putExtra("name", editName.getText().toString());
//        intent.putExtra("address", editAddress.getText().toString());

        intent.putExtra("person", person);
        startActivity(intent);
    }

    public void cancel(View v) {
        finish();
    }
}
