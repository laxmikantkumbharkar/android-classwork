package com.sunbeaminfo.application3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.sunbeaminfo.application3.adapter.MovieAdapter;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<String> movies = new ArrayList<>();
    RecyclerView recyclerView;
    MovieAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Inflating an xml file
        // - reading the xml file
        // - understanding the structure
        // - creating an object of every Widget class used in xml file
        // - building the hierarchy and returning the object of root element
        //
        // Inflater
        // - class/entity which inflates an xml
        // - types
        //   - LayoutInflater
        //   - MenuInflater


        // setContentView(R.layout.activity_main);
        // recyclerView = findViewById(R.id.recyclerView);

        // create the widget programmatically
        recyclerView = new RecyclerView(this);

        // add the recyclerview to the activity UI
        setContentView(recyclerView);

        movies.add("Toy Story I");
        movies.add("Cars 1");
        movies.add("Cars 2");
        movies.add("Cars 3");
        movies.add("Moana");

        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(manager);

        adapter = new MovieAdapter(movies, this);
        recyclerView.setAdapter(adapter);
    }
}
