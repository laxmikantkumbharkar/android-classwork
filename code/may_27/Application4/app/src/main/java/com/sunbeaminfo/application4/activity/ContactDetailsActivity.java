package com.sunbeaminfo.application4.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.sunbeaminfo.application4.R;
import com.sunbeaminfo.application4.model.Contact;

public class ContactDetailsActivity extends AppCompatActivity {

    TextView textName, textEmail, textPhone, textAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_details);

        textName = findViewById(R.id.textName);
        textEmail = findViewById(R.id.textEmail);
        textPhone = findViewById(R.id.textPhone);
        textAddress = findViewById(R.id.textAddress);

        Intent intent = getIntent();

        Contact contact = (Contact) intent.getSerializableExtra("contact");
        textName.setText(contact.getFullName());
        textEmail.setText(contact.getEmail());
        textPhone.setText(contact.getPhone());
        textAddress.setText(contact.getFullAddress());
    }
}
