package com.sunbeaminfo.application4.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.sunbeaminfo.application4.R;
import com.sunbeaminfo.application4.model.Contact;

import java.util.ArrayList;

public class AddContactActivity extends AppCompatActivity {

    EditText editFirstName, editLastName, editPhone, editEmail, editAddress, editZipcode;
    Spinner spinnerCity;

    String cities[] = { "Pune", "Satara", "Karad", "Nashik", "Mumbai", "Sangli", "Kolhapur" };

    ArrayAdapter<String> citiesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);

        editFirstName = findViewById(R.id.editFirstName);
        editLastName = findViewById(R.id.editLastName);
        editPhone = findViewById(R.id.editPhone);
        editEmail = findViewById(R.id.editEmail);
        editAddress = findViewById(R.id.editAddress);
        editZipcode = findViewById(R.id.editZipcode);
        spinnerCity = findViewById(R.id.spinnerCity);

        citiesAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, cities);
        spinnerCity.setAdapter(citiesAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_add_contact, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menuSave) {
            Contact contact = new Contact();
            contact.setFirstName(editFirstName.getText().toString());
            contact.setLastName(editLastName.getText().toString());
            contact.setAddress(editAddress.getText().toString());
            contact.setEmail(editEmail.getText().toString());
            contact.setPhone(editPhone.getText().toString());
            contact.setZipcode(editZipcode.getText().toString());

            int index = spinnerCity.getSelectedItemPosition();
            contact.setCity(cities[index]);

            Intent data = new Intent();
            data.putExtra("contact", contact);
            setResult(0, data);

            finish();
        } else if (id == R.id.menuClose) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
