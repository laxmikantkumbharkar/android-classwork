package com.sunbeaminfo.application4.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sunbeaminfo.application4.R;
import com.sunbeaminfo.application4.model.Contact;

import java.util.ArrayList;

public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ViewHolder> {

    private final Context context;
    private final ArrayList<Contact> contacts;
    private ClickListener listener;

    public interface ClickListener {
        void onItemClick(int index);
    }

    public ContactsAdapter(Context context, ArrayList<Contact> contacts) {
        this.context = context;
        this.contacts = contacts;

        // the ContactListActivity  has implemented ClickListener
        // context is referring to ContactsListActivity
        this.listener = (ClickListener) context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        LinearLayout layout = (LinearLayout) layoutInflater.inflate(R.layout.recyclerview_item_contact, null);
        return new ViewHolder(layout);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        final Contact contact = contacts.get(i);
        viewHolder.textName.setText(contact.getFullName());
        viewHolder.textAddress.setText(contact.getFullAddress());
        viewHolder.textPhone.setText(contact.getPhone());
        viewHolder.textEmail.setText(contact.getEmail());

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "contact: " + contact.getFullName(), Toast.LENGTH_SHORT).show();
                listener.onItemClick(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        View itemView;
        TextView textName, textEmail, textPhone, textAddress;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            this.itemView = itemView;
            textName = itemView.findViewById(R.id.textName);
            textEmail = itemView.findViewById(R.id.textEmail);
            textPhone = itemView.findViewById(R.id.textPhone);
            textAddress = itemView.findViewById(R.id.textAddress);
        }
    }
}
