package com.sunbeaminfo.application4.activity;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.LinearLayout;

import com.sunbeaminfo.application4.R;
import com.sunbeaminfo.application4.adapter.ContactsAdapter;
import com.sunbeaminfo.application4.model.Contact;

import java.util.ArrayList;

public class ContactListActivity extends AppCompatActivity implements ContactsAdapter.ClickListener {

    ArrayList<Contact> contacts = new ArrayList<>();

    RecyclerView recyclerView;
    ContactsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        // TODO: remove this after testing
        contacts.add(new Contact("steve", "jobs", "usa", "+123424", "steve@apple.com", "NY", "23434"));
        contacts.add(new Contact("bill", "gates", "usa", "+123424", "steve@apple.com", "NY", "23434"));
        contacts.add(new Contact("sundar", "pichchai", "india", "+914523424", "sundar@sunbeaminfo.com", "pune", "411041"));

        recyclerView = findViewById(R.id.recyclerView);

        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(manager);

        adapter = new ContactsAdapter(this, contacts);
        recyclerView.setAdapter(adapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_contact_list, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menuAddContact) {
            Intent intent = new Intent(this, AddContactActivity.class);
            startActivityForResult(intent, 0);
        } else if (id == R.id.menuClose) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            Contact contact = (Contact) data.getSerializableExtra("contact");
            contacts.add(contact);

            // refresh recyclerview
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onItemClick(int index) {
        Intent intent = new Intent(this, ContactDetailsActivity.class);
        intent.putExtra("contact", contacts.get(index));
        startActivity(intent);
    }

    /*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add("Add Contact");
        menu.add("Close");
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getTitle().equals("Add Contact")) {
            Intent intent = new Intent(this, AddContactActivity.class);
            startActivity(intent);
        } else if (item.getTitle().equals("Close")) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
    */
}
