package com.sunbeaminfo.application1.model;

import com.google.gson.JsonObject;

public class Cart {
    private int id;
    private int userId;
    private int itemId;
    private int quantity;
    private float rate;

    private Item item;

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public static Cart parse(JsonObject object) {
        Cart cart = new Cart();

        cart.id = object.get("cartId").getAsInt();
        cart.userId = object.get("userId").getAsInt();
        cart.itemId = object.get("itemId").getAsInt();
        cart.quantity = object.get("quantity").getAsInt();
        cart.rate = object.get("rate").getAsInt();

        cart.item = new Item();
        cart.item.setId(object.get("itemId").getAsInt());
        cart.item.setTitle(object.get("title").getAsString());
        cart.item.setDescription(object.get("description").getAsString());
        cart.item.setThumbnail(object.get("thumbnail").getAsString());
        cart.item.setUnit(object.get("unit").getAsString());
        cart.item.setCompany(object.get("company").getAsString());
        cart.item.setPrice(object.get("price").getAsFloat());


        return cart;
    }

    public Cart() {
    }

    public Cart(int id, int userId, int itemId, int quantity, float rate) {
        this.id = id;
        this.userId = userId;
        this.itemId = itemId;
        this.quantity = quantity;
        this.rate = rate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }
}
