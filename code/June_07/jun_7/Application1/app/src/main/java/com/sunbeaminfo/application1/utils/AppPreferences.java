package com.sunbeaminfo.application1.utils;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class AppPreferences {

    private static final String KEY_LOGIN_STATUS = "login_status";
    private static final String KEY_USER_ID = "user_id";
    private static final String KEY_USER_EMAIL = "user_email";
    private static final String KEY_NAME = "user_name";

    public static boolean isUserLoggedIn() {
        SharedPreferences preferences =
                PreferenceManager.getDefaultSharedPreferences(AppContext.getContext());
        return preferences.getBoolean(KEY_LOGIN_STATUS, false);
    }

    public static int getUserId() {
        SharedPreferences preferences =
                PreferenceManager.getDefaultSharedPreferences(AppContext.getContext());
        return preferences.getInt(KEY_USER_ID, 0);
    }

    public static void loginUser(String name, String email, int userId) {
        SharedPreferences preferences =
                PreferenceManager.getDefaultSharedPreferences(AppContext.getContext());
        preferences
            .edit()
            .putBoolean(KEY_LOGIN_STATUS, true)
            .putString(KEY_NAME, name)
            .putString(KEY_USER_EMAIL, email)
            .putInt(KEY_USER_ID, userId)
            .commit();
    }

    public static void logout() {
        SharedPreferences preferences =
                PreferenceManager.getDefaultSharedPreferences(AppContext.getContext());
        preferences
                .edit()
                .putBoolean(KEY_LOGIN_STATUS, false)
                .putString(KEY_NAME, "")
                .putString(KEY_USER_EMAIL, "")
                .putInt(KEY_USER_ID, -1)
                .commit();
    }
}
