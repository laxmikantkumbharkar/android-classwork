package com.sunbeaminfo.application1.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.sunbeaminfo.application1.R;
import com.sunbeaminfo.application1.model.Item;
import com.sunbeaminfo.application1.network.Network;
import com.sunbeaminfo.application1.utils.AppPreferences;
import com.sunbeaminfo.application1.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetailsActivity extends BaseActivity {

    @BindView(R.id.imageView) ImageView imageView;
    @BindView(R.id.textCompany) TextView textCompany;
    @BindView(R.id.textTitle) TextView textTitle;
    @BindView(R.id.textUnit) TextView textUnit;
    @BindView(R.id.textPrice) TextView textPrice;
    @BindView(R.id.buttonAdd) Button buttonAdd;
    @BindView(R.id.ratingBar) RatingBar ratingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);

        Item item = (Item) getIntent().getSerializableExtra("item");
        textTitle.setText(item.getTitle());
        textPrice.setText("" + item.getPrice());
        textCompany.setText(item.getCompany());
        textUnit.setText(item.getUnit());

        Network.loadImage(this, item.getThumbnail(), imageView);

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                Log.d("rating", "" + ratingBar.getRating());
                JsonObject body = new JsonObject();
                body.addProperty("userId", AppPreferences.getUserId());
                body.addProperty("itemId", item.getId());
                body.addProperty("rating", ratingBar.getRating());

                Network.with(Constants.ROUTE_ITEM + "/rating")
                    .method("POST")
                    .body(body)
                    .result(new Network.APIResultCallback() {
                        @Override
                        public void onSuccess(JsonElement result) {
                            Toast.makeText(DetailsActivity.this, "Updated rating", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onFailure() {
                            Toast.makeText(DetailsActivity.this, "Error while rating the item", Toast.LENGTH_SHORT).show();
                        }
                    });

            }
        });
    }

    @OnClick(R.id.buttonAdd)
    public void onViewClicked() {
    }
}
