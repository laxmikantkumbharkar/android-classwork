package com.sunbeaminfo.application1.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.sunbeaminfo.application1.R;
import com.sunbeaminfo.application1.model.Cart;
import com.sunbeaminfo.application1.network.Network;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> {

    private final Listener listener;

    public interface Listener {
        void onAdd(int index);
        void onRemove(int index);
    }

    private final Context context;
    private final ArrayList<Cart> cartItems;

    public CartAdapter(Context context, ArrayList<Cart> cartItems, Listener listener) {
        this.context = context;
        this.cartItems = cartItems;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.recyclerview_item_cart, null));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Cart cart = cartItems.get(i);

        viewHolder.textTitle.setText(cart.getItem().getTitle());
        viewHolder.textPrice.setText("" + cart.getItem().getPrice());
        viewHolder.textCompany.setText(cart.getItem().getCompany());
        viewHolder.textUnit.setText(cart.getItem().getUnit());
        viewHolder.textQuantity.setText("" + cart.getQuantity());

        Network.loadImage(context, cart.getItem().getThumbnail(), viewHolder.imageView);

        viewHolder.buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onAdd(i);
            }
        });

        viewHolder.buttonMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onRemove(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return cartItems.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder  {
        @BindView(R.id.imageView) ImageView imageView;
        @BindView(R.id.textCompany) TextView textCompany;
        @BindView(R.id.textTitle) TextView textTitle;
        @BindView(R.id.textUnit) TextView textUnit;
        @BindView(R.id.textPrice) TextView textPrice;
        @BindView(R.id.textQuantity) TextView textQuantity;

        @BindView(R.id.buttonAdd) Button buttonAdd;
        @BindView(R.id.buttonMinus) Button buttonMinus;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
