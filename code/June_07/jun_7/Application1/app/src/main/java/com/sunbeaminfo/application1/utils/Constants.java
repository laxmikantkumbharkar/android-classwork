package com.sunbeaminfo.application1.utils;

public class Constants {

    // for APIs
    public static final String SERVER_URL = "http://172.20.10.3:3000/";

    // routes
    public static final String ROUTE_ITEM = "item";
    public static final String ROUTE_CART = "cart";
    public static final String ROUTE_USER = "user";
}
