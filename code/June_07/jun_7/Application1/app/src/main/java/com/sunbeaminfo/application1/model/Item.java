package com.sunbeaminfo.application1.model;

import com.google.gson.JsonObject;

import java.io.Serializable;

public class Item implements Serializable {
    private int id;
    private String title;
    private String description;
    private String thumbnail;
    private String unit;
    private String company;
    private float price;
    private float rating;

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public static Item parse(JsonObject object) {
        Item item = new Item();

        item.id = object.get("id").getAsInt();
        item.title = object.get("title").getAsString();
        item.description = object.get("description").getAsString();
        item.thumbnail = object.get("thumbnail").getAsString();
        item.unit = object.get("unit").getAsString();
        item.company = object.get("company").getAsString();
        item.price = object.get("price").getAsFloat();
        item.rating = object.get("rating").getAsFloat();

        return item;
    }

    public Item() {
    }

    public Item(int id, String title, String description, String thumbnail, String unit, String company, float price) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.thumbnail = thumbnail;
        this.unit = unit;
        this.company = company;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getCompany() {
        return company.toUpperCase();
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
