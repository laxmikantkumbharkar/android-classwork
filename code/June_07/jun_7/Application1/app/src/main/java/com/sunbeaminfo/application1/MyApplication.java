package com.sunbeaminfo.application1;

import android.app.Application;

import com.sunbeaminfo.application1.utils.AppContext;

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        AppContext.setContext(this);
    }
}
