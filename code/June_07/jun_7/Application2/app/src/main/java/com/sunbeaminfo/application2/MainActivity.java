package com.sunbeaminfo.application2;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.editRows) EditText editRows;
    @BindView(R.id.editSeats) EditText editSeats;
    @BindView(R.id.buttonRender) Button buttonRender;
    @BindView(R.id.layoutSeats) LinearLayout layoutSeats;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.buttonRender)
    public void onViewClicked() {


        // refresh
        layoutSeats.removeAllViews();

        LayoutInflater inflater = getLayoutInflater();
        int rows = Integer.parseInt(editRows.getText().toString());
        int seatPerRow = Integer.parseInt(editSeats.getText().toString());

        LinearLayout.LayoutParams params =
                new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);

        LinearLayout.LayoutParams seatParams =
                new LinearLayout.LayoutParams(100, 100);
        seatParams.setMargins(10, 10, 10, 10);

        for (int rowIndex = 0; rowIndex < rows; rowIndex ++) {
            LinearLayout layout = new LinearLayout(this);
            layout.setOrientation(LinearLayout.HORIZONTAL);
            layout.setLayoutParams(params);

            for (int seatIndex = 0; seatIndex < seatPerRow; seatIndex ++) {
                TextView textView = (TextView) inflater.inflate(R.layout.layout_seat, null);
                textView.setLayoutParams(seatParams);
                textView.setText("" + (seatIndex + 1));
                layout.addView(textView);
            }

            layoutSeats.addView(layout);
        }
    }
}
