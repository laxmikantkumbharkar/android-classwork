package com.sunbeaminfo.application2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SeatLayoutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seat_layout);
    }

    private void getSeats() {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add("refresh");
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getTitle().equals("refresh")) {
            Ion.with(this)
                    .load("http://172.20.10.3:3000/seats")
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            if (result.get("status").getAsString().equals("success")) {
                                try {
                                    String config = result.get("data").getAsString();
                                    JSONArray array = new JSONArray(config);
                                    Log.d("config", array.toString());
                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                }

                            }
                        }
                    });
        }
        return super.onOptionsItemSelected(item);
    }
}
