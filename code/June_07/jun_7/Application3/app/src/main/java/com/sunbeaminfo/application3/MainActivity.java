package com.sunbeaminfo.application3;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.sunbeaminfo.application3.model.Multiplex;
import com.sunbeaminfo.application3.model.Row;
import com.sunbeaminfo.application3.model.Seat;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    LinearLayout layoutSeats;

    Multiplex multiplex = new Multiplex();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        layoutSeats = findViewById(R.id.layoutSeats);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add("refresh");
        return super.onCreateOptionsMenu(menu);
    }

    private void renderSeats() {
        layoutSeats.removeAllViews();

        // -1: missing
        // 1: available [white]
        // 2: not available [green]
        // 3: booked [gray]

        LayoutInflater inflater = getLayoutInflater();

        LinearLayout.LayoutParams params =
                new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);

        LinearLayout.LayoutParams seatParams =
                new LinearLayout.LayoutParams(100, 100);
        seatParams.setMargins(10, 10, 10, 10);

        for (int rowIndex = 0; rowIndex < multiplex.numRows(); rowIndex++) {
            Row row = multiplex.getRows().get(rowIndex);

            LinearLayout layout = new LinearLayout(MainActivity.this);
            layout.setOrientation(LinearLayout.HORIZONTAL);
            layout.setLayoutParams(params);

            for (int columnIndex = 0; columnIndex < row.getSeats().size(); columnIndex++) {
                final Seat seat = row.getSeats().get(columnIndex);

                if (seat.getStatus() == -1) {
                    TextView textView = (TextView) inflater.inflate(R.layout.layout_seat_missing, null);
                    textView.setLayoutParams(seatParams);
                    layout.addView(textView);
                } else {
                    final TextView textView = (TextView) inflater.inflate(R.layout.layout_seat, null);
                    textView.setLayoutParams(seatParams);
                    textView.setText("" + (columnIndex + 1));

                    // adjustment
                    final int rowNo = rowIndex;
                    final int seatNo = columnIndex;

                    int status = seat.getStatus();
                    if (status == 1) {
                        textView.setBackgroundColor(Color.WHITE);
                        textView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                int status = 0;
                                if (seat.getStatus() == 1) {
                                    status = 2;
                                    textView.setBackgroundColor(Color.GREEN);
                                } else {
                                    status = 1;
                                    textView.setBackgroundColor(Color.WHITE);
                                }

                                seat.setStatus(status);

                                JsonObject body = new JsonObject();
                                body.addProperty("userId", 1);
                                body.addProperty("rowNo", rowNo);
                                body.addProperty("seatNo", seatNo);
                                body.addProperty("status", status);


                                Ion.with(MainActivity.this)
                                    .load("POST", "http://172.20.10.3:3000/seat/booking")
                                    .setJsonObjectBody(body)
                                    .asJsonObject()
                                    .setCallback(new FutureCallback<JsonObject>() {
                                        @Override
                                        public void onCompleted(Exception e, JsonObject result) {
                                            if (result.get("status").getAsString().equals("success")) {
                                                Toast.makeText(MainActivity.this, "booked seat", Toast.LENGTH_SHORT).show();
                                            } else {
                                                Toast.makeText(MainActivity.this, "error while booking seat", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                            }
                        });
                    } else if (status == 2 || status == 3) {
                        textView.setBackgroundColor(Color.GRAY);
                    }
                    layout.addView(textView);
                }
            }

            layoutSeats.addView(layout);
        }
    }

    private void getBookings() {
        Ion.with(this)
            .load("http://172.20.10.3:3000/seat/booking")
            .asJsonObject()
            .setCallback(new FutureCallback<JsonObject>() {
                @Override
                public void onCompleted(Exception e, JsonObject result) {
                    if (result.get("status").getAsString().equals("success")) {
                        JsonArray array = result.get("data").getAsJsonArray();
                        for (int index = 0; index < array.size(); index++) {
                            JsonObject object = array.get(index).getAsJsonObject();
                            int rowNo = object.get("rowNo").getAsInt();
                            int seatNo = object.get("seatNo").getAsInt();
                            int status = object.get("status").getAsInt();

                            Row row = multiplex.getRows().get(rowNo);
                            Seat seat = row.getSeats().get(seatNo);
                            seat.setStatus(status);
                        }
                    }

                    // display all the seats
                    renderSeats();
                }
            });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getTitle().equals("refresh")) {
            Ion.with(this)
                    .load("http://172.20.10.3:3000/seat")
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            if (result.get("status").getAsString().equals("success")) {
                                multiplex.removeRows();

                                try {
                                    String config = result.get("data").getAsString();
                                    JSONArray array = new JSONArray(config);
                                    Log.d("config", array.toString());

                                    for (int index = 0; index < array.length(); index++) {
                                        JSONArray rows = (JSONArray) array.get(index);
                                        Row row = new Row();

                                        for (int columnIndex = 0; columnIndex < rows.length(); columnIndex++) {
                                            JSONObject object = (JSONObject) rows.get(columnIndex);

                                            Seat seat = new Seat();
                                            seat.setNumber(object.getInt("no"));
                                            seat.setStatus(object.getInt("status"));
                                            row.addSeat(seat);
                                        }

                                        multiplex.addRow(row);
                                    }
                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                }

                                // get the bookings
                                getBookings();
                            }
                        }
                    });
        }
        return super.onOptionsItemSelected(item);
    }
}
