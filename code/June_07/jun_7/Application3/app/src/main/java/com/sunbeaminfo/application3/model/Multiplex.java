package com.sunbeaminfo.application3.model;

import java.util.ArrayList;

public class Multiplex {
    private ArrayList<Row> rows = new ArrayList<>();

    public int numRows() {
        return rows.size();
    }

    public int numSeats(int index) {
        return rows.get(index).getSeats().size();
    }

    public ArrayList<Row> getRows() {
        return rows;
    }

    public void addRow(Row row) {
        rows.add(row);
    }

    public void removeRows() {
        rows.clear();
    }
}
