package com.sunbeaminfo.application3.model;

import java.util.ArrayList;

public class Row {
    private ArrayList<Seat> seats = new ArrayList<>();

    public Row() {

    }

    public ArrayList<Seat> getSeats() {
        return seats;
    }

    public void addSeat(Seat seat) {
        seats.add(seat);
    }

    public void removeSeats() {
        seats.clear();
    }
}

