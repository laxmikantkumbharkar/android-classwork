package com.sunbeaminfo.application3.model;

public class Seat {
    private int id;
    private int number;
    private int status;

    public Seat() {
    }

    public Seat(int id, int number, int status) {
        this.id = id;
        this.number = number;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
