package com.sunbeaminfo.myimdb.fragment;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.sunbeaminfo.myimdb.R;
import com.sunbeaminfo.myimdb.activity.MovieDetailsActivity;
import com.sunbeaminfo.myimdb.adapter.MoviesAdapter;
import com.sunbeaminfo.myimdb.model.Movie;
import com.sunbeaminfo.myimdb.utils.Constants;
import com.sunbeaminfo.myimdb.utils.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MovieListFragment extends Fragment implements MoviesAdapter.Listener {

    ProgressDialog dialog;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    Unbinder unbinder;

    ArrayList<Movie> movies = new ArrayList<>();
    MoviesAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dialog = new ProgressDialog(getActivity());
        dialog.setTitle("Please wait..");
        dialog.setMessage("Application is loading movies...");


        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.sunbeaminfo.myimdb.service.MOVIE_ADDED");

        BroadcastReceiver receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d("MovieListFragment", "received broadcast");
                getMovies();
            }
        };

        getActivity().registerReceiver(receiver, intentFilter);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movie_list, null);
        unbinder = ButterKnife.bind(this, view);

        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        adapter = new MoviesAdapter(getActivity(), movies, this);
        recyclerView.setAdapter(adapter);
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        getMovies();
    }

    private void getMovies() {
        final String url = Utils.getUrl(Constants.ROUTE_MOVIE);

        dialog.show();

        Ion
            .with(getActivity())
            .load(url)
            .asJsonObject()
            .setCallback(new FutureCallback<JsonObject>() {
                @Override
                public void onCompleted(Exception e, JsonObject result) {
                    if (e != null) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), "Error while loading the movies", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.d("response", result.toString());
                        if (result.get("status").getAsString().equals("success")) {
                            movies.clear();

                            JsonArray array = result.get("data").getAsJsonArray();
                            for (int index = 0; index < array.size(); index++) {
                                JsonObject object = array.get(index).getAsJsonObject();

                                Movie movie = new Movie();
                                movie.setId(object.get("id").getAsInt());
                                movie.setTitle(object.get("title").getAsString());
                                movie.setYear(object.get("year").getAsInt());
                                movie.setRating(object.get("rating").getAsFloat());
                                movie.setThumbnail(object.get("thumbnail").getAsString());
                                movie.setLength(object.get("length").getAsString());

                                movies.add(movie);
                            }

                            adapter.notifyDataSetChanged();

                            dialog.dismiss();
                        } else {
                            Toast.makeText(getActivity(), "Error while loading the movies", Toast.LENGTH_SHORT).show();
                        }
                    }

                }
            });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onClick(int index) {
        Movie movie = movies.get(index);

        Intent intent = new Intent(getActivity(), MovieDetailsActivity.class);
        intent.putExtra("id", movie.getId());
        startActivity(intent);
    }
}
