package com.sunbeaminfo.myimdb.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.sunbeaminfo.myimdb.R;
import com.sunbeaminfo.myimdb.model.Movie;
import com.sunbeaminfo.myimdb.utils.Constants;
import com.sunbeaminfo.myimdb.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieDetailsActivity extends AppCompatActivity {

    @BindView(R.id.imageView) ImageView imageView;
    @BindView(R.id.textTitle) TextView textTitle;
    @BindView(R.id.textGenre) TextView textGenre;
    @BindView(R.id.textYear) TextView textYear;
    @BindView(R.id.textLength) TextView textLength;
    @BindView(R.id.textRating) TextView textRating;
    @BindView(R.id.textDirectors) TextView textDirectors;
    @BindView(R.id.textWriters) TextView textWriters;
    @BindView(R.id.textStars) TextView textStars;
    @BindView(R.id.textShortDescription) TextView textShortDescription;
    @BindView(R.id.textStoryline) TextView textStoryline;

    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);
        ButterKnife.bind(this);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Details");
        actionBar.setDisplayHomeAsUpEnabled(true);

        dialog = new ProgressDialog(this);
        dialog.setTitle("please wait..");
        dialog.setMessage("Application is downloading the movie details");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void showDetails(Movie movie) {
        textTitle.setText(movie.getTitle());
        textGenre.setText(movie.getGenre());
        textYear.setText("" + movie.getYear());
        textRating.setText("" + movie.getRating());
        textDirectors.setText(movie.getDirectors());
        textWriters.setText(movie.getWriters());
        textStars.setText(movie.getStars());
        textShortDescription.setText(movie.getShortDescription());
        textStoryline.setText(movie.getStoryline());

        String imageUrl = Utils.getImageUrl(movie.getThumbnail());
        Ion.with(this)
            .load(imageUrl)
            .withBitmap()
            .intoImageView(imageView);

        dialog.dismiss();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getMovieDetails();
    }

    private void getMovieDetails() {
        int id = getIntent().getIntExtra("id", 0);

        String url = Utils.getUrl(Constants.ROUTE_MOVIE + "/" + id);

        dialog.show();
        Ion.with(this)
                .load(url)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        Log.d("details", result.toString());
                        if (result.get("status").getAsString().equals("success")) {
                            JsonObject object = result.get("data").getAsJsonObject();

                            Movie movie = new Movie();
                            movie.setId(object.get("id").getAsInt());
                            movie.setTitle(object.get("title").getAsString());
                            movie.setYear(object.get("year").getAsInt());
                            movie.setShortDescription(object.get("shortDescription").getAsString());
                            movie.setDirectors(object.get("directors").getAsString());
                            movie.setWriters(object.get("writers").getAsString());
                            movie.setStars(object.get("stars").getAsString());
                            movie.setGenre(object.get("genre").getAsString());
                            movie.setStoryline(object.get("storyline").getAsString());
                            movie.setRating(object.get("rating").getAsFloat());
                            movie.setThumbnail(object.get("thumbnail").getAsString());
                            movie.setLength(object.get("length").getAsString());

                            showDetails(movie);
                        }
                    }
                });
    }
}
