package com.sunbeaminfo.myimdb.service;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.sunbeaminfo.myimdb.R;
import com.sunbeaminfo.myimdb.activity.MainActivity;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "FCMService";

    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        // send broadcast message
        Log.d(TAG, "sending broadcast");

        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("com.sunbeaminfo.myimdb.service.MOVIE_ADDED");
        sendBroadcast(broadcastIntent);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), "movie_channel");
        builder.setContentTitle("New movie added");
        builder.setSmallIcon(R.drawable.icon);
        builder.setContentText("New Movie added by administrator. Please watach the movie");
        builder.setTicker("new movie added");

        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, 0);
        builder.setContentIntent(pendingIntent);

        NotificationChannel channel = new NotificationChannel("movie_channel", "movie_channel", NotificationManager.IMPORTANCE_DEFAULT);
        channel.setDescription("this is a movie channel");

        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        manager.createNotificationChannel(channel);
        manager.notify(1, builder.build());

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }
    }

}
