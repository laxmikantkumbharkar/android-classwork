package com.sunbeaminfo.myimdb.adapter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.sunbeaminfo.myimdb.fragment.MovieListFragment;
import com.sunbeaminfo.myimdb.fragment.SearchMoviesFragment;

public class MainActivityFragmentsAdapter extends FragmentPagerAdapter {

    MovieListFragment movieListFragment = new MovieListFragment();
    SearchMoviesFragment searchMoviesFragment = new SearchMoviesFragment();

    public MainActivityFragmentsAdapter(FragmentManager fm) {
        super(fm);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        String title = "";
        if (position == 0) {
            title = "Movies";
        } else if (position == 1) {
            title = "Search";
        }
        return title;
    }

    @Override
    public Fragment getItem(int i) {
        Fragment fragment = null;
        if (i == 0) {
            fragment = movieListFragment;
        } else if (i == 1) {
            fragment = searchMoviesFragment;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
