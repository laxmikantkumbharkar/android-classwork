package com.sunbeaminfo.myimdb.adapter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.sunbeaminfo.myimdb.fragment.MovieListFragment;

public class MainActivityFragmentsAdapter extends FragmentPagerAdapter {

    MovieListFragment movieListFragment = new MovieListFragment();

    public MainActivityFragmentsAdapter(FragmentManager fm) {
        super(fm);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        String title = "";
        if (position == 0) {
            title = "Movies";
        }
        return title;
    }

    @Override
    public Fragment getItem(int i) {
        Fragment fragment = null;
        if (i == 0) {
            fragment = movieListFragment;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 1;
    }
}
