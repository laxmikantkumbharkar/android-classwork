package com.sunbeaminfo.application2;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import com.sunbeaminfo.application2.db.SQLiteHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends AppCompatActivity {

    @BindView(R.id.editName) EditText editName;
    @BindView(R.id.editAddress) EditText editAddress;
    @BindView(R.id.editEmail) EditText editEmail;
    @BindView(R.id.editPassword) EditText editPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.buttonRegister)
    public void onButtonRegisterClicked() {
        SQLiteHelper helper = new SQLiteHelper(this);
        SQLiteDatabase db = helper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("name", editName.getText().toString());
        values.put("email", editEmail.getText().toString());
        values.put("address", editAddress.getText().toString());
        values.put("password", editPassword.getText().toString());

        db.insert("User", null, values);

        db.close();
        finish();
    }

    @OnClick(R.id.buttonCancel)
    public void onButtonCancelClicked() {
        finish();
    }
}
