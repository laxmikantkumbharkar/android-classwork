package com.sunbeaminfo.application2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.sunbeaminfo.application2.db.SQLiteHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.editEmail) EditText editEmail;
    @BindView(R.id.editPassword) EditText editPassword;
    @BindView(R.id.checkboxRememberMe) CheckBox checkboxRememberMe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.buttonLogin)
    public void onButtonLoginClicked() {
        SQLiteHelper helper = new SQLiteHelper(this);
        SQLiteDatabase db = helper.getReadableDatabase();

        Cursor cursor = db.rawQuery("select id, name, email, password, address from User where email = ? and password = ?",
                new String[]{editEmail.getText().toString(), editPassword.getText().toString()});

        if (!cursor.isAfterLast()) {
            // cursor is NOT empty

            cursor.moveToFirst();

            int id = cursor.getInt(0);
            String name = cursor.getString(1);

            /*
            boolean rememberMe = false;
            if (checkboxRememberMe.isChecked()) {
                rememberMe = true;
            }
            */

            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            preferences
                    .edit()
                    .putBoolean("login_status", checkboxRememberMe.isChecked())
                    .putString("name", name)
                    .putInt("id", id)
                    .commit();

            startActivity(new Intent(this, MainActivity.class));
            finish();
        } else {
            // cursor is empty
            Toast.makeText(this, "Invalid email or password", Toast.LENGTH_SHORT).show();
        }

        db.close();
    }

    @OnClick(R.id.buttonRegister)
    public void onButtonRegisterClicked() {
        startActivity(new Intent(this, RegisterActivity.class));
    }
}
