package com.sunbeaminfo.application4;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;

import com.sunbeaminfo.application4.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.editTitle) EditText editTitle;
    @BindView(R.id.editYear) EditText editYear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.buttonInsert)
    public void onButtonInsertClicked() {
        ContentResolver resolver = getContentResolver();

        ContentValues  values = new ContentValues();
        values.put("title", editTitle.getText().toString());
        values.put("year", editYear.getText().toString());

        resolver.insert(Constants.CONTENT_URI, values);
    }

    @OnClick(R.id.buttonQuery)
    public void onButtonQueryClicked() {
        String columns[] = {"id", "title", "year"};
        Cursor cursor = getContentResolver().query(Constants.CONTENT_URI, columns, null, null, null);
        if (!cursor.isAfterLast()) {
            cursor.moveToFirst();

            while(!cursor.isAfterLast()) {

                int id = cursor.getInt(cursor.getColumnIndex("id"));
                String title = cursor.getString(cursor.getColumnIndex("title"));
                int year = cursor.getInt(cursor.getColumnIndex("year"));
                Log.d("Id: ",  "" + id);
                Log.d("Year: ",  "" + year);
                Log.d("Title", title);

                cursor.moveToNext();
            }
        }
        cursor.close();
    }
}
