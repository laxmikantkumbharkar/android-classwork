package com.sunbeaminfo.application4.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.sunbeaminfo.application4.db.SQLIteHelper;

public class MyContentProvider extends ContentProvider {


    SQLiteDatabase db;

    @Override
    public boolean onCreate() {
        SQLIteHelper helper = new SQLIteHelper(getContext());
        db = helper.getWritableDatabase();
        return false;
    }

    
    @Override
    public Cursor query(Uri uri,  String[] projection,  String selection,  String[] selectionArgs,  String sortOrder) {
        Cursor cursor = db.query("Movie", projection, selection, selectionArgs, null, null, sortOrder);
        return cursor;
    }

    
    @Override
    public String getType(Uri uri) {
        return null;
    }

    
    @Override
    public Uri insert(Uri uri,  ContentValues values) {
        db.insert("Movie", null, values);
        return null;
    }

    @Override
    public int delete(Uri uri,  String selection,  String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri,  ContentValues values,  String selection,  String[] selectionArgs) {
        return 0;
    }
}
