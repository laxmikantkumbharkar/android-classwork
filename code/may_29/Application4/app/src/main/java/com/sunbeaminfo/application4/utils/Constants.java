package com.sunbeaminfo.application4.utils;

import android.net.Uri;

public class Constants {
    public static final Uri CONTENT_URI = Uri.parse("content://com.sunbeaminfo.application4");
}
