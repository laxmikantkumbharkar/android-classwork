package com.sunbeaminfo.application3;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.editName) EditText editName;
    @BindView(R.id.editAddress) EditText editAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.buttonInsert)
    public void onInsertClicked() {
        ContentResolver resolver = getContentResolver();

        ContentValues values = new ContentValues();
        values.put("name", editName.getText().toString());
        values.put("address", editAddress.getText().toString());

        resolver.insert(Uri.parse("content://com.sunbeaminfo.application3"), values);

    }

    @OnClick(R.id.buttonQuery)
    public void onQueryClicked() {
        ContentResolver resolver = getContentResolver();
        String columns[] = {"id", "name", "address"};
        Cursor cursor = resolver.query(Uri.parse("content://com.sunbeaminfo.application3"), columns, null, null, null);
        if (!cursor.isAfterLast()) {
            cursor.moveToFirst();

            while(!cursor.isAfterLast()) {
                int id = cursor.getInt(0);
                String name = cursor.getString(1);
                String address = cursor.getString(2);

                Log.d("Id", "" + id);
                Log.d("Name", name);
                Log.d("Address", address);

                cursor.moveToNext();
            }
        }
    }
}
