package com.sunbeaminfo.application3.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.sunbeaminfo.application3.db.SQLiteHelper;

public class ContactContentProvider extends ContentProvider {

    SQLiteDatabase db;

    @Override
    public boolean onCreate() {
        // connect to db


        // Activity -> this
        // ContentProvider -> getContext()
        // Service -> getApplicationContext()
        // BroadcastReceiver -> context will be sent as a param in onReceive()
        // Fragment -> getActivity()

        SQLiteHelper helper = new SQLiteHelper(getContext());
        db = helper.getWritableDatabase();
        return false;
    }

    @Override
    public String getType( Uri uri) {
        return null;
    }

    
    @Override
    public Cursor query( Uri uri,  String[] projection,  String selection,  String[] selectionArgs,  String sortOrder) {
        Cursor cursor = db.query("Contact", projection, selection, selectionArgs, null, null, sortOrder);
        return cursor;
    }

    
    @Override
    public Uri insert( Uri uri,  ContentValues values) {
        db.insert("Contact", null, values);
        return null;
    }

    @Override
    public int delete( Uri uri,  String selection,  String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update( Uri uri,  ContentValues values,  String selection,  String[] selectionArgs) {
        return 0;
    }
}
