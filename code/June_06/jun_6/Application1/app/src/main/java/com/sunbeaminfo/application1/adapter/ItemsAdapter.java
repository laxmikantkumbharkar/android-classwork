package com.sunbeaminfo.application1.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.sunbeaminfo.application1.R;
import com.sunbeaminfo.application1.model.Item;
import com.sunbeaminfo.application1.network.Network;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemsAdapter extends RecyclerView.Adapter<ItemsAdapter.ViewHolder> {

    private final Listener listener;

    public interface Listener {
        void onAdd(int index);
    }

    private final Context context;
    private final ArrayList<Item> items;


    public ItemsAdapter(Context context, ArrayList<Item> items, Listener listener) {
        this.context = context;
        this.items = items;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.recyclerview_item, null));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        Item item = items.get(i);

        viewHolder.textTitle.setText(item.getTitle());
        viewHolder.textPrice.setText("" + item.getPrice());
        viewHolder.textCompany.setText(item.getCompany());
        viewHolder.textUnit.setText(item.getUnit());

        Network.loadImage(context, item.getThumbnail(), viewHolder.imageView);

        viewHolder.buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onAdd(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.textCompany) TextView textCompany;
        @BindView(R.id.textTitle) TextView textTitle;
        @BindView(R.id.textUnit) TextView textUnit;
        @BindView(R.id.textPrice) TextView textPrice;
        @BindView(R.id.imageView) ImageView imageView;
        @BindView(R.id.buttonAdd) Button buttonAdd;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }
}
