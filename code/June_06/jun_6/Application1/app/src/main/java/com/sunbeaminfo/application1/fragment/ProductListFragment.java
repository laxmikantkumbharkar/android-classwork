package com.sunbeaminfo.application1.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.sunbeaminfo.application1.R;
import com.sunbeaminfo.application1.adapter.ItemsAdapter;
import com.sunbeaminfo.application1.model.Item;
import com.sunbeaminfo.application1.network.Network;
import com.sunbeaminfo.application1.utils.Constants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ProductListFragment extends Fragment implements ItemsAdapter.Listener {

    ArrayList<Item> items = new ArrayList<>();
    ItemsAdapter adapter;

    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    Unbinder unbinder;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_list, null);
        unbinder = ButterKnife.bind(this, view);

        adapter = new ItemsAdapter(getActivity(), items, this);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        recyclerView.setAdapter(adapter);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getItems();
    }

    private void getItems() {

        Network.with(getActivity(), Constants.ROUTE_ITEM)
            .result(new Network.APIResultCallback() {
                @Override
                public void onSuccess(JsonElement result) {
                    items.clear();

                    JsonArray array  = (JsonArray) result;
                    for (int index = 0; index < array.size(); index++) {
                        JsonObject object = array.get(index).getAsJsonObject();
                        items.add(Item.parse(object));
                    }

                    adapter.notifyDataSetChanged();
                }

                @Override
                public void onFailure() {
                    Toast.makeText(getActivity(), "call failed", Toast.LENGTH_SHORT).show();
                }
            });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onAdd(int index) {
        Item item = items.get(index);

        JsonObject body = new JsonObject();
        body.addProperty("userId", 1);
        body.addProperty("itemId", item.getId());
        body.addProperty("price", item.getPrice());

        Network.with(getActivity(), Constants.ROUTE_CART)
            .method("POST")
            .body(body)
            .result(new Network.APIResultCallback() {
                @Override
                public void onSuccess(JsonElement result) {
                    Toast.makeText(getActivity(), "Added item to cart", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure() {
                    Toast.makeText(getActivity(), "call failed", Toast.LENGTH_SHORT).show();
                }
            });
    }
}
