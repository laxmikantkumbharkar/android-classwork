package com.sunbeaminfo.application1.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.sunbeaminfo.application1.R;
import com.sunbeaminfo.application1.adapter.CartAdapter;
import com.sunbeaminfo.application1.model.Cart;
import com.sunbeaminfo.application1.model.Item;
import com.sunbeaminfo.application1.network.Network;
import com.sunbeaminfo.application1.utils.Constants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class CartFragment extends Fragment implements CartAdapter.Listener {

    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.textPrice) TextView textPrice;
    Unbinder unbinder;
    CartAdapter adapter;

    ArrayList<Cart> cartItems = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cart, null);
        unbinder = ButterKnife.bind(this, view);

        adapter = new CartAdapter(getActivity(), cartItems, this);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        recyclerView.setAdapter(adapter);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getItems();
    }

    private void getItems() {

        Network.with(getActivity(), Constants.ROUTE_CART + "/1")
                .result(new Network.APIResultCallback() {
                    @Override
                    public void onSuccess(JsonElement result) {
                        cartItems.clear();

                        float total = 0;

                        JsonArray array  = (JsonArray) result;
                        for (int index = 0; index < array.size(); index++) {
                            JsonObject object = array.get(index).getAsJsonObject();
                            Cart cart = Cart.parse(object);
                            cartItems.add(cart);

                            total += cart.getQuantity() * cart.getRate();
                        }


                        textPrice.setText("₹ " + total);
                        adapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onFailure() {
                        Toast.makeText(getActivity(), "call failed", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.buttonCheckout)
    public void onViewClicked() {
    }

    @Override
    public void onAdd(int index) {
        Cart item = cartItems.get(index);
        int quantity = item.getQuantity() + 1;
        updateQuantity(item.getId(), quantity);
    }

    private void updateQuantity(int id, int quantity) {
        JsonObject body = new JsonObject();
        body.addProperty("quantity", quantity);

        Network.with(getActivity(),Constants.ROUTE_CART + "/" + id)
                .method("PUT")
                .body(body)
                .result(new Network.APIResultCallback() {
                    @Override
                    public void onSuccess(JsonElement result) {
                        getItems();
                    }

                    @Override
                    public void onFailure() {
                    }
                });
    }

    @Override
    public void onRemove(int index) {
        Cart item = cartItems.get(index);
        int quantity = item.getQuantity() - 1;

        if (quantity == 0) {
            // delete the item
            Network.with(getActivity(),Constants.ROUTE_CART + "/" + item.getId())
                    .method("DELETE")
                    .result(new Network.APIResultCallback() {
                        @Override
                        public void onSuccess(JsonElement result) {
                            getItems();
                        }

                        @Override
                        public void onFailure() {
                        }
                    });
        } else {
            updateQuantity(item.getId(), quantity);
        }

    }
}
