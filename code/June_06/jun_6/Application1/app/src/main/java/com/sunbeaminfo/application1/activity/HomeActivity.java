package com.sunbeaminfo.application1.activity;

import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.view.MenuItem;
import android.widget.TextView;

import com.sunbeaminfo.application1.R;
import com.sunbeaminfo.application1.fragment.CartFragment;
import com.sunbeaminfo.application1.fragment.ProductListFragment;

public class HomeActivity extends AppCompatActivity {
    ProductListFragment productListFragment = new ProductListFragment();
    CartFragment cartFragment = new CartFragment();

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.frameLayout, productListFragment)
                        .commit();
                    return true;
                case R.id.navigation_dashboard:
                    return true;
                case R.id.navigation_notifications:
                    return true;
                case R.id.navigation_search:
                    return true;
                case R.id.navigation_cart:
                    getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.frameLayout, cartFragment)
                        .commit();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        getSupportFragmentManager()
            .beginTransaction()
            .replace(R.id.frameLayout, productListFragment)
            .commit();
    }

}
