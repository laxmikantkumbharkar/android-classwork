package com.sunbeaminfo.application1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.sunbeaminfo.application1.fragment.ProductListFragment;

public class MainActivity extends AppCompatActivity {

    ProductListFragment productListFragment = new ProductListFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initFragment();
    }

    private void initFragment() {
        getSupportFragmentManager()
            .beginTransaction()
            .replace(R.id.frameLayout, productListFragment)
            .commit();
    }
}
